d  <Q                         _ADDITIONAL_LIGHTS     _ADDITIONAL_LIGHT_SHADOWS      _MAIN_LIGHT_SHADOWS_CASCADE    
   _NORMALMAP  	X  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_LightData;
	UNITY_UNIFORM vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM vec4 unity_SHAr;
	UNITY_UNIFORM vec4 unity_SHAg;
	UNITY_UNIFORM vec4 unity_SHAb;
	UNITY_UNIFORM vec4 unity_SHBr;
	UNITY_UNIFORM vec4 unity_SHBg;
	UNITY_UNIFORM vec4 unity_SHBb;
	UNITY_UNIFORM vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 _BaseMap_ST;
	UNITY_UNIFORM vec4 _DetailAlbedoMap_ST;
	UNITY_UNIFORM vec4 _BaseColor;
	UNITY_UNIFORM vec4 _SpecColor;
	UNITY_UNIFORM vec4 _EmissionColor;
	UNITY_UNIFORM float _Cutoff;
	UNITY_UNIFORM float _Smoothness;
	UNITY_UNIFORM float _Metallic;
	UNITY_UNIFORM float _BumpScale;
	UNITY_UNIFORM float _Parallax;
	UNITY_UNIFORM float _OcclusionStrength;
	UNITY_UNIFORM float _ClearCoatMask;
	UNITY_UNIFORM float _ClearCoatSmoothness;
	UNITY_UNIFORM float _DetailAlbedoMapScale;
	UNITY_UNIFORM float _DetailNormalMapScale;
	UNITY_UNIFORM float _Surface;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TANGENT0;
in highp vec2 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD4;
out highp vec3 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
bool u_xlatb6;
void main()
{
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _BaseMap_ST.xy + _BaseMap_ST.zw;
    vs_TEXCOORD1.xyz = vec3(0.0, 0.0, 0.0);
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat6 = max(u_xlat6, 1.17549435e-38);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD3.xyz = vec3(u_xlat6) * u_xlat1.xyz;
    u_xlat1.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_TANGENT0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_TANGENT0.zzz + u_xlat1.xyz;
    u_xlat6 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat6 = max(u_xlat6, 1.17549435e-38);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD4.xyz = vec3(u_xlat6) * u_xlat1.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb6 = !!(unity_WorldTransformParams.w>=0.0);
#else
    u_xlatb6 = unity_WorldTransformParams.w>=0.0;
#endif
    u_xlat6 = (u_xlatb6) ? 1.0 : -1.0;
    vs_TEXCOORD4.w = u_xlat6 * in_TANGENT0.w;
    u_xlat1.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb6 = !!(unity_OrthoParams.w==0.0);
#else
    u_xlatb6 = unity_OrthoParams.w==0.0;
#endif
    vs_TEXCOORD5.x = (u_xlatb6) ? u_xlat1.x : hlslcc_mtx4x4unity_MatrixV[0].z;
    vs_TEXCOORD5.y = (u_xlatb6) ? u_xlat1.y : hlslcc_mtx4x4unity_MatrixV[1].z;
    vs_TEXCOORD5.z = (u_xlatb6) ? u_xlat1.z : hlslcc_mtx4x4unity_MatrixV[2].z;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0[4];
uniform 	vec4 _MainLightPosition;
uniform 	vec4 _MainLightColor;
uniform 	vec4 _AdditionalLightsCount;
uniform 	vec4 _AdditionalLightsPosition[32];
uniform 	vec4 _AdditionalLightsColor[32];
uniform 	vec4 _AdditionalLightsAttenuation[32];
uniform 	vec4 _AdditionalLightsSpotDir[32];
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
uniform 	vec4 _CascadeShadowSplitSpheres0;
uniform 	vec4 _CascadeShadowSplitSpheres1;
uniform 	vec4 _CascadeShadowSplitSpheres2;
uniform 	vec4 _CascadeShadowSplitSpheres3;
uniform 	vec4 _CascadeShadowSplitSphereRadii;
uniform 	vec4 _MainLightShadowParams;
uniform 	vec4 _AdditionalShadowParams[32];
uniform 	vec4 hlslcc_mtx4x4_AdditionalLightsWorldToShadow[128];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_LightData;
	UNITY_UNIFORM vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM vec4 unity_SHAr;
	UNITY_UNIFORM vec4 unity_SHAg;
	UNITY_UNIFORM vec4 unity_SHAb;
	UNITY_UNIFORM vec4 unity_SHBr;
	UNITY_UNIFORM vec4 unity_SHBg;
	UNITY_UNIFORM vec4 unity_SHBb;
	UNITY_UNIFORM vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 _BaseMap_ST;
	UNITY_UNIFORM vec4 _DetailAlbedoMap_ST;
	UNITY_UNIFORM vec4 _BaseColor;
	UNITY_UNIFORM vec4 _SpecColor;
	UNITY_UNIFORM vec4 _EmissionColor;
	UNITY_UNIFORM float _Cutoff;
	UNITY_UNIFORM float _Smoothness;
	UNITY_UNIFORM float _Metallic;
	UNITY_UNIFORM float _BumpScale;
	UNITY_UNIFORM float _Parallax;
	UNITY_UNIFORM float _OcclusionStrength;
	UNITY_UNIFORM float _ClearCoatMask;
	UNITY_UNIFORM float _ClearCoatSmoothness;
	UNITY_UNIFORM float _DetailAlbedoMapScale;
	UNITY_UNIFORM float _DetailNormalMapScale;
	UNITY_UNIFORM float _Surface;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(1) uniform mediump sampler2D _BaseMap;
UNITY_LOCATION(2) uniform mediump sampler2D _BumpMap;
UNITY_LOCATION(3) uniform mediump sampler2D _MainLightShadowmapTexture;
UNITY_LOCATION(4) uniform mediump sampler2DShadow hlslcc_zcmp_MainLightShadowmapTexture;
UNITY_LOCATION(5) uniform mediump sampler2D _AdditionalLightsShadowmapTexture;
UNITY_LOCATION(6) uniform mediump sampler2DShadow hlslcc_zcmp_AdditionalLightsShadowmapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD3;
in highp vec4 vs_TEXCOORD4;
in highp vec3 vs_TEXCOORD5;
layout(location = 0) out highp vec4 SV_Target0;
vec4 u_xlat0;
bool u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
vec3 u_xlat3;
vec4 u_xlat4;
vec3 u_xlat5;
bvec4 u_xlatb5;
vec4 u_xlat6;
vec4 u_xlat7;
vec4 u_xlat8;
vec3 u_xlat9;
vec4 u_xlat10;
bvec3 u_xlatb10;
vec3 u_xlat12;
float u_xlat13;
bool u_xlatb13;
vec3 u_xlat17;
uint u_xlatu17;
vec3 u_xlat18;
vec3 u_xlat20;
bvec3 u_xlatb20;
float u_xlat24;
float u_xlat29;
float u_xlat31;
float u_xlat33;
int u_xlati33;
uint u_xlatu33;
float u_xlat35;
bool u_xlatb35;
float u_xlat36;
uint u_xlatu36;
float u_xlat37;
int u_xlati37;
bool u_xlatb37;
float u_xlat38;
float u_xlat40;
int u_xlati40;
float u_xlat41;
bool u_xlatb41;
void main()
{
ImmCB_0[0] = vec4(1.0,0.0,0.0,0.0);
ImmCB_0[1] = vec4(0.0,1.0,0.0,0.0);
ImmCB_0[2] = vec4(0.0,0.0,1.0,0.0);
ImmCB_0[3] = vec4(0.0,0.0,0.0,1.0);
    u_xlat0 = texture(_BaseMap, vs_TEXCOORD0.xy);
    u_xlat1 = u_xlat0.wxyz * _BaseColor.wxyz;
    u_xlat2.xyz = texture(_BumpMap, vs_TEXCOORD0.xy).xyw;
    u_xlat2.x = u_xlat2.x * u_xlat2.z;
    u_xlat2.xy = u_xlat2.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat33 = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat33 = min(u_xlat33, 1.0);
    u_xlat33 = (-u_xlat33) + 1.0;
    u_xlat33 = sqrt(u_xlat33);
    u_xlat33 = max(u_xlat33, 1.00000002e-16);
    u_xlat2.xy = u_xlat2.xy * vec2(vec2(_BumpScale, _BumpScale));
    u_xlat24 = dot(vs_TEXCOORD5.xyz, vs_TEXCOORD5.xyz);
    u_xlat24 = max(u_xlat24, 1.17549435e-38);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat3.xyz = vec3(u_xlat24) * vs_TEXCOORD5.xyz;
    u_xlat4.xyz = vs_TEXCOORD3.zxy * vs_TEXCOORD4.yzx;
    u_xlat4.xyz = vs_TEXCOORD3.yzx * vs_TEXCOORD4.zxy + (-u_xlat4.xyz);
    u_xlat4.xyz = u_xlat4.xyz * vs_TEXCOORD4.www;
    u_xlat4.xyz = u_xlat2.yyy * u_xlat4.xyz;
    u_xlat2.xyw = u_xlat2.xxx * vs_TEXCOORD4.xyz + u_xlat4.xyz;
    u_xlat2.xyw = vec3(u_xlat33) * vs_TEXCOORD3.xyz + u_xlat2.xyw;
    u_xlat33 = dot(u_xlat2.xyw, u_xlat2.xyw);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat4.xyz = vec3(u_xlat33) * u_xlat2.xyw;
    u_xlat2.xyw = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres0.xyz);
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres1.xyz);
    u_xlat6.xyz = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres2.xyz);
    u_xlat7.xyz = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres3.xyz);
    u_xlat8.x = dot(u_xlat2.xyw, u_xlat2.xyw);
    u_xlat8.y = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat8.z = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat8.w = dot(u_xlat7.xyz, u_xlat7.xyz);
    u_xlatb5 = lessThan(u_xlat8, _CascadeShadowSplitSphereRadii);
    u_xlat6.x = u_xlatb5.x ? float(1.0) : 0.0;
    u_xlat6.y = u_xlatb5.y ? float(1.0) : 0.0;
    u_xlat6.z = u_xlatb5.z ? float(1.0) : 0.0;
    u_xlat6.w = u_xlatb5.w ? float(1.0) : 0.0;
;
    u_xlat2.x = (u_xlatb5.x) ? float(-1.0) : float(-0.0);
    u_xlat2.y = (u_xlatb5.y) ? float(-1.0) : float(-0.0);
    u_xlat2.w = (u_xlatb5.z) ? float(-1.0) : float(-0.0);
    u_xlat2.xyw = u_xlat2.xyw + u_xlat6.yzw;
    u_xlat6.yzw = max(u_xlat2.xyw, vec3(0.0, 0.0, 0.0));
    u_xlat33 = dot(u_xlat6, vec4(4.0, 3.0, 2.0, 1.0));
    u_xlat33 = (-u_xlat33) + 4.0;
    u_xlatu33 = uint(u_xlat33);
    u_xlati33 = int(int(u_xlatu33) << 2);
    u_xlat2.xyw = vs_TEXCOORD2.yyy * hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati33 + 1)].xyz;
    u_xlat2.xyw = hlslcc_mtx4x4_MainLightWorldToShadow[u_xlati33].xyz * vs_TEXCOORD2.xxx + u_xlat2.xyw;
    u_xlat2.xyw = hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati33 + 2)].xyz * vs_TEXCOORD2.zzz + u_xlat2.xyw;
    u_xlat2.xyw = u_xlat2.xyw + hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati33 + 3)].xyz;
    u_xlat4.w = 1.0;
    u_xlat5.x = dot(unity_SHAr, u_xlat4);
    u_xlat5.y = dot(unity_SHAg, u_xlat4);
    u_xlat5.z = dot(unity_SHAb, u_xlat4);
    u_xlat6 = u_xlat4.yzzx * u_xlat4.xyzz;
    u_xlat7.x = dot(unity_SHBr, u_xlat6);
    u_xlat7.y = dot(unity_SHBg, u_xlat6);
    u_xlat7.z = dot(unity_SHBb, u_xlat6);
    u_xlat33 = u_xlat4.y * u_xlat4.y;
    u_xlat33 = u_xlat4.x * u_xlat4.x + (-u_xlat33);
    u_xlat6.xyz = unity_SHC.xyz * vec3(u_xlat33) + u_xlat7.xyz;
    u_xlat5.xyz = u_xlat5.xyz + u_xlat6.xyz;
    u_xlat5.xyz = max(u_xlat5.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat33 = (-_Metallic) * 0.959999979 + 0.959999979;
    u_xlat36 = (-u_xlat33) + 1.0;
    u_xlat12.xyz = vec3(u_xlat33) * u_xlat1.yzw;
    u_xlat0.xyz = u_xlat0.xyz * _BaseColor.xyz + vec3(-0.0399999991, -0.0399999991, -0.0399999991);
    u_xlat0.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat0.xyz + vec3(0.0399999991, 0.0399999991, 0.0399999991);
    u_xlat33 = (-_Smoothness) + 1.0;
    u_xlat37 = u_xlat33 * u_xlat33;
    u_xlat37 = max(u_xlat37, 0.0078125);
    u_xlat38 = u_xlat37 * u_xlat37;
    u_xlat36 = u_xlat36 + _Smoothness;
#ifdef UNITY_ADRENO_ES3
    u_xlat36 = min(max(u_xlat36, 0.0), 1.0);
#else
    u_xlat36 = clamp(u_xlat36, 0.0, 1.0);
#endif
    u_xlat6.x = u_xlat37 * 4.0 + 2.0;
    vec3 txVec0 = vec3(u_xlat2.xy,u_xlat2.w);
    u_xlat2.x = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec0, 0.0);
    u_xlat13 = (-_MainLightShadowParams.x) + 1.0;
    u_xlat2.x = u_xlat2.x * _MainLightShadowParams.x + u_xlat13;
#ifdef UNITY_ADRENO_ES3
    u_xlatb13 = !!(0.0>=u_xlat2.w);
#else
    u_xlatb13 = 0.0>=u_xlat2.w;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb35 = !!(u_xlat2.w>=1.0);
#else
    u_xlatb35 = u_xlat2.w>=1.0;
#endif
    u_xlatb13 = u_xlatb35 || u_xlatb13;
    u_xlat2.x = (u_xlatb13) ? 1.0 : u_xlat2.x;
    u_xlat17.xyz = vs_TEXCOORD2.xyz + (-_WorldSpaceCameraPos.xyz);
    u_xlat13 = dot(u_xlat17.xyz, u_xlat17.xyz);
    u_xlat13 = u_xlat13 * _MainLightShadowParams.z + _MainLightShadowParams.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat13 = min(max(u_xlat13, 0.0), 1.0);
#else
    u_xlat13 = clamp(u_xlat13, 0.0, 1.0);
#endif
    u_xlat13 = u_xlat13 * u_xlat13;
    u_xlat35 = (-u_xlat2.x) + 1.0;
    u_xlat2.x = u_xlat13 * u_xlat35 + u_xlat2.x;
    u_xlat35 = dot((-u_xlat3.xyz), u_xlat4.xyz);
    u_xlat35 = u_xlat35 + u_xlat35;
    u_xlat17.xyz = u_xlat4.xyz * (-vec3(u_xlat35)) + (-u_xlat3.xyz);
    u_xlat35 = dot(u_xlat4.xyz, u_xlat3.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat35 = min(max(u_xlat35, 0.0), 1.0);
#else
    u_xlat35 = clamp(u_xlat35, 0.0, 1.0);
#endif
    u_xlat35 = (-u_xlat35) + 1.0;
    u_xlat35 = u_xlat35 * u_xlat35;
    u_xlat35 = u_xlat35 * u_xlat35;
    u_xlat7.x = (-u_xlat33) * 0.699999988 + 1.70000005;
    u_xlat33 = u_xlat33 * u_xlat7.x;
    u_xlat33 = u_xlat33 * 6.0;
    u_xlat7 = textureLod(unity_SpecCube0, u_xlat17.xyz, u_xlat33);
    u_xlat33 = u_xlat7.w + -1.0;
    u_xlat33 = unity_SpecCube0_HDR.w * u_xlat33 + 1.0;
    u_xlat33 = max(u_xlat33, 0.0);
    u_xlat33 = log2(u_xlat33);
    u_xlat33 = u_xlat33 * unity_SpecCube0_HDR.y;
    u_xlat33 = exp2(u_xlat33);
    u_xlat33 = u_xlat33 * unity_SpecCube0_HDR.x;
    u_xlat17.xyz = u_xlat7.xyz * vec3(u_xlat33);
    u_xlat7.xy = vec2(u_xlat37) * vec2(u_xlat37) + vec2(-1.0, 1.0);
    u_xlat33 = float(1.0) / u_xlat7.y;
    u_xlat18.xyz = (-u_xlat0.xyz) + vec3(u_xlat36);
    u_xlat18.xyz = vec3(u_xlat35) * u_xlat18.xyz + u_xlat0.xyz;
    u_xlat18.xyz = vec3(u_xlat33) * u_xlat18.xyz;
    u_xlat17.xyz = u_xlat17.xyz * u_xlat18.xyz;
    u_xlat5.xyz = u_xlat5.xyz * u_xlat12.xyz + u_xlat17.xyz;
    u_xlat33 = u_xlat2.x * unity_LightData.z;
    u_xlat2.x = dot(u_xlat4.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat33 = u_xlat33 * u_xlat2.x;
    u_xlat17.xyz = vec3(u_xlat33) * _MainLightColor.xyz;
    u_xlat2.xzw = vs_TEXCOORD5.xyz * vec3(u_xlat24) + _MainLightPosition.xyz;
    u_xlat33 = dot(u_xlat2.xzw, u_xlat2.xzw);
    u_xlat33 = max(u_xlat33, 1.17549435e-38);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat2.xzw = vec3(u_xlat33) * u_xlat2.xzw;
    u_xlat33 = dot(u_xlat4.xyz, u_xlat2.xzw);
#ifdef UNITY_ADRENO_ES3
    u_xlat33 = min(max(u_xlat33, 0.0), 1.0);
#else
    u_xlat33 = clamp(u_xlat33, 0.0, 1.0);
#endif
    u_xlat2.x = dot(_MainLightPosition.xyz, u_xlat2.xzw);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat33 = u_xlat33 * u_xlat33;
    u_xlat33 = u_xlat33 * u_xlat7.x + 1.00001001;
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat33 = u_xlat33 * u_xlat33;
    u_xlat2.x = max(u_xlat2.x, 0.100000001);
    u_xlat33 = u_xlat33 * u_xlat2.x;
    u_xlat33 = u_xlat6.x * u_xlat33;
    u_xlat33 = u_xlat38 / u_xlat33;
    u_xlat2.xzw = u_xlat0.xyz * vec3(u_xlat33) + u_xlat12.xyz;
    u_xlat2.xzw = u_xlat2.xzw * u_xlat17.xyz + u_xlat5.xyz;
    u_xlat33 = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlatu33 =  uint(int(u_xlat33));
    u_xlat5.xyz = u_xlat2.xzw;
    for(uint u_xlatu_loop_1 = uint(0u) ; u_xlatu_loop_1<u_xlatu33 ; u_xlatu_loop_1++)
    {
        u_xlati37 = int(uint(u_xlatu_loop_1 & 3u));
        u_xlatu17 = uint(u_xlatu_loop_1 >> 2u);
        u_xlat37 = dot(unity_LightIndices[int(u_xlatu17)], ImmCB_0[u_xlati37]);
        u_xlati37 = int(u_xlat37);
        u_xlat17.xyz = (-vs_TEXCOORD2.xyz) * _AdditionalLightsPosition[u_xlati37].www + _AdditionalLightsPosition[u_xlati37].xyz;
        u_xlat18.x = dot(u_xlat17.xyz, u_xlat17.xyz);
        u_xlat18.x = max(u_xlat18.x, 6.10351563e-05);
        u_xlat29 = inversesqrt(u_xlat18.x);
        u_xlat8.xyz = u_xlat17.xyz * vec3(u_xlat29);
        u_xlat40 = float(1.0) / float(u_xlat18.x);
        u_xlat18.x = u_xlat18.x * _AdditionalLightsAttenuation[u_xlati37].x;
        u_xlat18.x = (-u_xlat18.x) * u_xlat18.x + 1.0;
        u_xlat18.x = max(u_xlat18.x, 0.0);
        u_xlat18.x = u_xlat18.x * u_xlat18.x;
        u_xlat18.x = u_xlat18.x * u_xlat40;
        u_xlat40 = dot(_AdditionalLightsSpotDir[u_xlati37].xyz, u_xlat8.xyz);
        u_xlat40 = u_xlat40 * _AdditionalLightsAttenuation[u_xlati37].z + _AdditionalLightsAttenuation[u_xlati37].w;
#ifdef UNITY_ADRENO_ES3
        u_xlat40 = min(max(u_xlat40, 0.0), 1.0);
#else
        u_xlat40 = clamp(u_xlat40, 0.0, 1.0);
#endif
        u_xlat40 = u_xlat40 * u_xlat40;
        u_xlat18.x = u_xlat40 * u_xlat18.x;
        u_xlati40 = int(_AdditionalShadowParams[u_xlati37].w);
#ifdef UNITY_ADRENO_ES3
        u_xlatb41 = !!(u_xlati40<0);
#else
        u_xlatb41 = u_xlati40<0;
#endif
        if(u_xlatb41){
            u_xlat9.x = 1.0;
        }
        if(!u_xlatb41){
#ifdef UNITY_ADRENO_ES3
            u_xlatb41 = !!(vec4(0.0, 0.0, 0.0, 0.0)!=vec4(_AdditionalShadowParams[u_xlati37].z));
#else
            u_xlatb41 = vec4(0.0, 0.0, 0.0, 0.0)!=vec4(_AdditionalShadowParams[u_xlati37].z);
#endif
            if(u_xlatb41){
                u_xlatb20.xyz = greaterThanEqual(abs(u_xlat8.zzyy), abs(u_xlat8.xyxx)).xyz;
                u_xlatb41 = u_xlatb20.y && u_xlatb20.x;
                u_xlatb10.xyz = lessThan((-u_xlat8.zyxz), vec4(0.0, 0.0, 0.0, 0.0)).xyz;
                u_xlat20.x = (u_xlatb10.x) ? float(5.0) : float(4.0);
                u_xlat20.y = (u_xlatb10.y) ? float(3.0) : float(2.0);
                u_xlat10.x = u_xlatb10.z ? 1.0 : float(0.0);
                u_xlat31 = (u_xlatb20.z) ? u_xlat20.y : u_xlat10.x;
                u_xlat41 = (u_xlatb41) ? u_xlat20.x : u_xlat31;
                u_xlat20.x = trunc(_AdditionalShadowParams[u_xlati37].w);
                u_xlat41 = u_xlat41 + u_xlat20.x;
                u_xlati40 = int(u_xlat41);
            }
            u_xlati40 = int(u_xlati40 << 2);
            u_xlat10 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati40 + 1)];
            u_xlat10 = hlslcc_mtx4x4_AdditionalLightsWorldToShadow[u_xlati40] * vs_TEXCOORD2.xxxx + u_xlat10;
            u_xlat10 = hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati40 + 2)] * vs_TEXCOORD2.zzzz + u_xlat10;
            u_xlat10 = u_xlat10 + hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati40 + 3)];
            u_xlat20.xyz = u_xlat10.xyz / u_xlat10.www;
            vec3 txVec1 = vec3(u_xlat20.xy,u_xlat20.z);
            u_xlat40 = textureLod(hlslcc_zcmp_AdditionalLightsShadowmapTexture, txVec1, 0.0);
            u_xlat41 = 1.0 + (-_AdditionalShadowParams[u_xlati37].x);
            u_xlat40 = u_xlat40 * _AdditionalShadowParams[u_xlati37].x + u_xlat41;
#ifdef UNITY_ADRENO_ES3
            u_xlatb41 = !!(0.0>=u_xlat20.z);
#else
            u_xlatb41 = 0.0>=u_xlat20.z;
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb20.x = !!(u_xlat20.z>=1.0);
#else
            u_xlatb20.x = u_xlat20.z>=1.0;
#endif
            u_xlatb41 = u_xlatb41 || u_xlatb20.x;
            u_xlat9.x = (u_xlatb41) ? 1.0 : u_xlat40;
        }
        u_xlat40 = (-u_xlat9.x) + 1.0;
        u_xlat40 = u_xlat13 * u_xlat40 + u_xlat9.x;
        u_xlat18.x = u_xlat40 * u_xlat18.x;
        u_xlat40 = dot(u_xlat4.xyz, u_xlat8.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat40 = min(max(u_xlat40, 0.0), 1.0);
#else
        u_xlat40 = clamp(u_xlat40, 0.0, 1.0);
#endif
        u_xlat18.x = u_xlat40 * u_xlat18.x;
        u_xlat9.xyz = u_xlat18.xxx * _AdditionalLightsColor[u_xlati37].xyz;
        u_xlat17.xyz = u_xlat17.xyz * vec3(u_xlat29) + u_xlat3.xyz;
        u_xlat37 = dot(u_xlat17.xyz, u_xlat17.xyz);
        u_xlat37 = max(u_xlat37, 1.17549435e-38);
        u_xlat37 = inversesqrt(u_xlat37);
        u_xlat17.xyz = vec3(u_xlat37) * u_xlat17.xyz;
        u_xlat37 = dot(u_xlat4.xyz, u_xlat17.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat37 = min(max(u_xlat37, 0.0), 1.0);
#else
        u_xlat37 = clamp(u_xlat37, 0.0, 1.0);
#endif
        u_xlat17.x = dot(u_xlat8.xyz, u_xlat17.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat17.x = min(max(u_xlat17.x, 0.0), 1.0);
#else
        u_xlat17.x = clamp(u_xlat17.x, 0.0, 1.0);
#endif
        u_xlat37 = u_xlat37 * u_xlat37;
        u_xlat37 = u_xlat37 * u_xlat7.x + 1.00001001;
        u_xlat17.x = u_xlat17.x * u_xlat17.x;
        u_xlat37 = u_xlat37 * u_xlat37;
        u_xlat17.x = max(u_xlat17.x, 0.100000001);
        u_xlat37 = u_xlat37 * u_xlat17.x;
        u_xlat37 = u_xlat6.x * u_xlat37;
        u_xlat37 = u_xlat38 / u_xlat37;
        u_xlat17.xyz = u_xlat0.xyz * vec3(u_xlat37) + u_xlat12.xyz;
        u_xlat5.xyz = u_xlat17.xyz * u_xlat9.xyz + u_xlat5.xyz;
    }
    SV_Target0.xyz = u_xlat5.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(_Surface==1.0);
#else
    u_xlatb0 = _Surface==1.0;
#endif
    SV_Target0.w = (u_xlatb0) ? u_xlat1.x : 1.0;
    return;
}

#endif
                                $Globals�        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                     0      _AdditionalLightsColor                    0     _AdditionalLightsAttenuation                  0     _AdditionalLightsSpotDir                  0     _WorldSpaceCameraPos                  0     _CascadeShadowSplitSpheres0                   �	     _CascadeShadowSplitSpheres1                   �	     _CascadeShadowSplitSpheres2                   �	     _CascadeShadowSplitSpheres3                   �	     _CascadeShadowSplitSphereRadii                    �	     _MainLightShadowParams                    �	     _AdditionalShadowParams                   �	     _MainLightWorldToShadow                 @     _AdditionalLightsWorldToShadow                   �         UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          UnityPerMaterial|         _BaseMap_ST                          _DetailAlbedoMap_ST                      
   _BaseColor                        
   _SpecColor                    0      _EmissionColor                    @      _Cutoff                   P      _Smoothness                   T   	   _Metallic                     X   
   _BumpScale                    \   	   _Parallax                     `      _OcclusionStrength                    d      _ClearCoatMask                    h      _ClearCoatSmoothness                  l      _DetailAlbedoMapScale                     p      _DetailNormalMapScale                     t      _Surface                  x          $Globals�         _WorldSpaceCameraPos                         unity_OrthoParams                           unity_MatrixV                           unity_MatrixVP                   `             unity_SpecCube0                   _BaseMap                _BumpMap                _MainLightShadowmapTexture               !   _AdditionalLightsShadowmapTexture                   UnityPerDraw              UnityPerMaterial             I   use of potentially uninitialized variable (AdditionalLightRealtimeShadow)   �  Compiling Vertex program with _ADDITIONAL_LIGHTS _ADDITIONAL_LIGHT_SHADOWS _MAIN_LIGHT_SHADOWS_CASCADE _NORMALMAP
Platform defines: SHADER_API_DESKTOP UNITY_ENABLE_DETAIL_NORMALMAP UNITY_ENABLE_REFLECTION_BUFFERS UNITY_LIGHTMAP_FULL_HDR UNITY_LIGHT_PROBE_PROXY_VOLUME UNITY_PBS_USE_BRDF1 UNITY_SPECCUBE_BLENDING UNITY_SPECCUBE_BOX_PROJECTION UNITY_USE_DITHER_MASK_FOR_ALPHABLENDED_SHADOWS
Disabled keywords: DIRLIGHTMAP_COMBINED FOG_EXP FOG_EXP2 FOG_LINEAR INSTANCING_ON LIGHTMAP_ON LIGHTMAP_SHADOW_MIXING SHADER_API_GLES30 SHADOWS_SHADOWMASK UNITY_ASTC_NORMALMAP_ENCODING UNITY_COLORSPACE_GAMMA UNITY_ENABLE_NATIVE_SHADOW_LOOKUPS UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS UNITY_HARDWARE_TIER1 UNITY_HARDWARE_TIER2 UNITY_HARDWARE_TIER3 UNITY_LIGHTMAP_DLDR_ENCODING UNITY_LIGHTMAP_RGBM_ENCODING UNITY_METAL_SHADOWS_USE_POINT_FILTERING UNITY_NO_DXT5nm UNITY_NO_FULL_STANDARD_SHADER UNITY_NO_SCREENSPACE_SHADOWS UNITY_PBS_USE_BRDF2 UNITY_PBS_USE_BRDF3 UNITY_PRETRANSFORM_TO_DISPLAY_ORIENTATION UNITY_VIRTUAL_TEXTURING _ADDITIONAL_LIGHTS_VERTEX _DETAIL_MULX2 _DETAIL_SCALED _MAIN_LIGHT_SHADOWS _MAIN_LIGHT_SHADOWS_SCREEN _PARALLAXMAP _RECEIVE_SHADOWS_OFF k   projects/Jumper/Library/PackageCache/com.unity.render-pipelines.universal@11.0.0/ShaderLibrary/Shadows.hlsl 	   E     