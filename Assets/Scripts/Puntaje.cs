using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Puntaje : MonoBehaviour
{
    public TextMeshProUGUI textoPuntosPro;
    public int puntos;
 

    private void Update()
    {
        textoPuntosPro.text = "PUNTOS: " + puntos.ToString();
    }
}
