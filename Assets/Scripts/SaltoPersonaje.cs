using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoPersonaje : MonoBehaviour
{
    Rigidbody rb;
    public float VelSalto = 10;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * VelSalto, ForceMode.Impulse);
        }

    }

}
